# VirusTracker

Small additional tag to the mobile phone application allowing to trace the posterior spread of a virus. It will also make it possible to trace the indirect spread of the virus by placing them in strategic fixed places (ATMs, elevators, etc.).